#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#ifndef C_STEP
#define C_STEP 4
#endif

#ifndef R_STEP
#define R_STEP 6
#endif

#define GET_PIXEL(image, row, col) CV_MAT_ELEM((*image), uint8_t, (row), (col))

typedef struct {
	size_t rows;
	size_t cols;
	char   pixel[];
} aa_t;

/* write converted ascii-art to a file */
int
export(aa_t *ascii, char *output)
{
	int i, j;
	FILE *fp;

	fp = fopen(output, "w");
	if (fp == NULL) {
		perror("fopen");
		return -1;
	}

	for (i=0; i<ascii->rows; i++) {
		for (j=0; j<ascii->cols; j++) {
			fprintf(fp, "%c", ascii->pixel[i * ascii->cols + j]);
		}
		fprintf(fp, "\n");
	}

	return 0;
}

/* convert an greyscale image to ascii-art */
int
ascii_art(CvMat *image, aa_t *ascii)
{
	int i, j;
	uint8_t pixel;
	uint8_t  min, max;
	int rows = (image->rows + R_STEP - 1) / R_STEP;
	int cols = (image->cols + C_STEP - 1) / C_STEP;

	char g2c[] = {'@', '8', 'O', 'o', ':', '.'};

	ascii = (aa_t*) malloc(sizeof(aa_t) + rows * cols);
	if (ascii == NULL) {
		perror("malloc");
		return -1;
	}

	ascii->rows = rows;
	ascii->cols = cols;

	min = max = CV_MAT_ELEM(*image, uint8_t, 0, 0);
	for (i=0; i<image->rows; i++) {
		for (j=0; j<image->cols; j++) {
			pixel = GET_PIXEL(image, i, j);
			if (pixel > max) max = pixel;
			if (pixel < min) min = pixel;
		}
	}

	for (i=0; i<image->rows; i+=R_STEP) {
		for (j=0; j<image->cols; j+=C_STEP) {
			int m, n;
			float gray = 0;
			int count = 0;

			/* get the sum */
			for (m=0; m<R_STEP; m++) {
				for (n=0; n<C_STEP; n++) {
					if ((i+m < image->rows) && (j+n < image->cols)) {
						gray += GET_PIXEL(image, i+m, j+n);
						count++;
					}
				}
			}
			
			/* get the average */
			gray /= count;

			/*
			 * scale to [0, 7].
			 * for clarity, we didn't check whether max-min is zero
			  */
			gray = 7 * (gray - min) / (max - min);

			/* map the grayscale to ascii character */
			ascii->pixel[i*cols/R_STEP + j/C_STEP] = g2c[(int)gray];
		}
	}

	return 0;
}

int
main(int argc, char **argv)
{
	/* original image */
	CvMat *image;

	/* ascii art */
	aa_t  *ascii;

	char *input, *output;

	if (argc != 3) {
		fprintf(stderr, "Usage: aa <input_image> <output.txt>\n");
		return -1;
	}

	input  = argv[1];
	output = argv[2];

	/* load the image */
	image = cvLoadImageM(input, 0);
	if (image == NULL) {
		fprintf(stderr, "Error: Failed to load '%s', abort.\n", input);
		return -1;
	}

	/* convert to ascii art */
	ascii_art(image, ascii);

	/* save the ascii art */
	export(ascii, output);

	return 0;
}
